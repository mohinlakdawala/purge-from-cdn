<?php

namespace App\Services\ContentDeliveryNetwork;


interface ContentDeliveryNetworkManager
{
    public function purge(string $Url);
}
