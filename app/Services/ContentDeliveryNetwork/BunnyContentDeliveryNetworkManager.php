<?php


namespace App\Services\ContentDeliveryNetwork;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class BunnyContentDeliveryNetworkManager implements ContentDeliveryNetworkManager
{
    /**
     * Client to handle http requests to BunnyCDN API.
     *
     * @var PendingRequest
     */
    protected $client;

    public function __construct()
    {
        $this->client = Http::withToken(config('services.bunny_cdn.token'));
    }

    public function purge(string $url)
    {
        return $this->client->post(
            sprintf(
                "%s/purge?%s",
                $this->baseUrl(),
                http_build_query(['url' => $url])
            )
        );
    }

    protected function baseUrl(): ?string
    {
        return config('services.bunny_cdn.base_url');
    }
}
