<?php

namespace App\Console\Commands;

use App\Services\ContentDeliveryNetwork\ContentDeliveryNetworkManager;
use Illuminate\Console\Command;

class PurgeCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purge:cache {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purge cache from CDN for the given file';

    /**
     * @var ContentDeliveryNetworkManager
     */
    private $contentDeliveryNetworkManager;

    /**
     * Create a new command instance.
     *
     * @param ContentDeliveryNetworkManager $contentDeliveryNetworkManager
     */
    public function __construct(ContentDeliveryNetworkManager $contentDeliveryNetworkManager)
    {
        parent::__construct();
        $this->contentDeliveryNetworkManager = $contentDeliveryNetworkManager;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info(sprintf("Attempting to purge file: %s", $this->argument('file')));
        $response = $this->contentDeliveryNetworkManager->purge($this->argument('file'));

        if ($response->ok()) {
            $this->info('Successfully purged.');
            return 0;
        }

        $this->error('Could not complete the request at this time.');
        return 1;
    }
}
