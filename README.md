## Laravel


### Installation

This is a standard laravel project so you should follow the same steps to install this in your local development environment.

### Setting up .env file
- Following variable need to be added in the `.env` file for BunnyCDN integration.
```env
BUNNYCDN_API_TOKEN=YOUR_TOKEN_HERE
```

### Additions
- A new console command `PurgeCache` has been created.

### PurgeCache command
- Use to purge the cache from the CDN provider
- Fire of the command using the below syntax whenever needed:
`php artisan purge:cache {file}`. The file name is mandatory since this command will purge cache for individual files
- The filename should be the CDN version of the file e.g. for BunnyCDN it should be `http://mohin-test-zone.b-cdn.net/assets/static/logo.jpg`.
So the final command will be like:
`php artisan purge:cache http://mohin-test-zone.b-cdn.net/assets/static/logo.jpg`
- This will be purge the `logo.jpg` file from the CDN cache.

### Implementation details
- Current implementation supports BunnyCDN provider. However, the command has been implemented in such a way that new CDN providers can be easily integrated and switched in future if it's needed.
- There is an interface created for `ContentDeliveryNetworkManager` that will ensure all functions necessary to interact with the CDN are implemented.
- There is a concrete class called `BunnyContentDeliveryNetworkManager` that implements this interface and provides the `purge` method which can handle puring of files from the BunnyCDN cache.
- Classes for other CDNs in future can be added easily and if they implement this interface, can easily be configured with Laravel's container bindings.



